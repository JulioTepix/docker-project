# Docker Project by TEPIXTLE Julio

- Julio Cesar Tepixtle Hernández

Premièrement il faut savoir que le projet dockerisé a été créé avec le framework Laravel 7

## Getting Started

1. Apres cloner le projet on exécute le suivante code pour créer l'image app avec les configurations nécessaires.

```
 docker-compose build
```

2. Créer et initialiser les containeurs.

```
 docker-compose up
```

3. il faut installer les dépendances du projet

```
 docker-compose exec app composer install
```

4. Pour vérifier que tout est bien installé on exécute
```
 docker-compose exec app php artisan config:cache
```

5. il faut taper sur le navigateur [localhost:82](http://localhost:82/) pour ouvrir phpmyadmin et vérifier si la base de données a été créé.

Username: root
Password: root

6. Créer les migrations nécessaires:
```
 docker-compose exec app php artisan migrate
```

7. Copier et coller dans la base de données, le code SQL qui se trouve dans le fichier gym.sql pour créer les tableaux qui manquent

8. il faut aller sur le navigateur [localhost:81](http://localhost:81/) pour ouvrir le site web du projet.

9. Registrer un nouveau utilisateur.

## Le projet

Pour pouvoir dockeriser ce projet j'ai eu besoin de :

1. Un serveur pour afficher le site web automatiquement. C'est pour ça que j'utilise nginx comme serveur, après il fallait le configurer. Le fichier de configuration se trouve sur le chemin nginx/conf.d/app.conf
et le port désigné est le 81.

2. Une base de données MySql pour que le projet marche

3. Phpmyadmin pour pouvoir travailler sur la base de données d'une manière plus rapide au moment de vérifier certains choses sur celle-ci.

4. PHP version 7.4 pour exécuter le projet et les commandes nécessaires.
Pour ce conteneur il fallait faire un fichier DockerFile pour le configurer en avance.