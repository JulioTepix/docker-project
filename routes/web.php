<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'clientes'], function(){

    Route::get('/index',[
        'as' => 'clientes.index',
        'uses' => 'ClientesController@index'
    ]);

    Route::get('/visitas',[
        'as' => 'clientes.visitas',
        'uses' => 'ClientesController@visitas'
    ]);

    Route::get('/create',[
        'as' => 'clientes.create',
        'uses' => 'ClientesController@create'
    ]);

    Route::post('/store',[
        'as' => 'clientes.store',
        'uses' => 'ClientesController@store'
    ]);

    Route::get('/{id}/show',[
        'as' => 'clientes.show',
        'uses' => 'ClientesController@show'
    ]);

    Route::get('/{id}/update',[
        'as' => 'clientes.update',
        'uses' => 'ClientesController@update'
    ]);

    Route::put('/{id}/edit',[
        'as' => 'clientes.edit',
        'uses' => 'ClientesController@edit'
    ]);

    Route::get('/{id}/delete',[
        'as' => 'clientes.delete',
        'uses' => 'ClientesController@delete'
    ]);

    Route::post('/destroy',[
        'as' => 'clientes.destroy',
        'uses' => 'ClientesController@destroy'
    ]);
    
});

Route::group(['prefix'=>'ganancias'], function(){

    Route::get('/index',[
        'as' => 'ganancias.index',
        'uses' => 'GananciasController@index'
    ]);
});

Route::group(['prefix'=>'suscripciones'], function(){

    Route::get('/index',[
        'as' => 'suscripciones.index',
        'uses' => 'SuscripcionController@index'
    ]);

    Route::get('/create',[
        'as' => 'suscripciones.create',
        'uses' => 'SuscripcionController@create'
    ]);

    Route::post('/store',[
        'as' => 'suscripciones.store',
        'uses' => 'SuscripcionController@store'
    ]);

    Route::get('/{id}/show',[
        'as' => 'suscripciones.show',
        'uses' => 'SuscripcionController@show'
    ]);

    Route::get('/{id}/update',[
        'as' => 'suscripciones.update',
        'uses' => 'SuscripcionController@update'
    ]);

    Route::put('/{id}/edit',[
        'as' => 'suscripciones.edit',
        'uses' => 'SuscripcionController@edit'
    ]);

    Route::get('/{id}/delete',[
        'as' => 'suscripciones.delete',
        'uses' => 'SuscripcionController@delete'
    ]);

    Route::post('/destroy',[
        'as' => 'suscripciones.destroy',
        'uses' => 'SuscripcionController@destroy'
    ]);
    
});

Route::group(['prefix'=>'productos'], function(){
    Route::get('/index',[
        'as' => 'productos.index',
        'uses' => 'ProductosController@index'
    ]);

    Route::get('/create',[
        'as' => 'productos.create',
        'uses' => 'ProductosController@create'
    ]);

    Route::post('/store',[
        'as' => 'productos.store',
        'uses' => 'ProductosController@store'
    ]);

    Route::get('/{id}/show',[
        'as' => 'productos.show',
        'uses' => 'ProductosController@show'
    ]);

    Route::get('/{id}/update',[
        'as' => 'productos.update',
        'uses' => 'ProductosController@update'
    ]);

    Route::put('/{id}/edit',[
        'as' => 'productos.edit',
        'uses' => 'ProductosController@edit'
    ]);

    Route::get('/{id}/delete',[
        'as' => 'productos.delete',
        'uses' => 'ProductosController@delete'
    ]);

    Route::post('/destroy',[
        'as' => 'productos.destroy',
        'uses' => 'ProductosController@destroy'
    ]);
    
});

Route::group(['prefix'=>'ventas'], function(){
    Route::get('/index',[
        'as' => 'ventas.index',
        'uses' => 'VentasController@index'
    ]);

    Route::get('/create',[
        'as' => 'ventas.create',
        'uses' => 'VentasController@create'
    ]);

    Route::post('/store',[
        'as' => 'ventas.store',
        'uses' => 'VentasController@store'
    ]);

    Route::get('/{id}/show',[
        'as' => 'ventas.show',
        'uses' => 'VentasController@show'
    ]);

    Route::get('/{id}/update',[
        'as' => 'ventas.update',
        'uses' => 'VentasController@update'
    ]);

    Route::put('/{id}/edit',[
        'as' => 'ventas.edit',
        'uses' => 'VentasController@edit'
    ]);

    Route::get('/{id}/delete',[
        'as' => 'ventas.delete',
        'uses' => 'VentasController@delete'
    ]);

    Route::post('/destroy',[
        'as' => 'ventas.destroy',
        'uses' => 'VentasController@destroy'
    ]);
    
});
