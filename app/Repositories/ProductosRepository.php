<?php
namespace App\Repositories;
use App\Models\Producto;
use DB;

class ProductosRepository{
    public function all_2(){
        return Producto::all();
    }

    public function all(){
        return DB::table('producto')->where('estado', '=', 1)->get();
    }

    public function search($value){
        return Producto::where([['nombre','like', "%$value%"],['estado','=',1]])
        ->orWhere([['marca','like', "%$value%"],['estado','=',1]])
        ->orderby('nombre')->get();
    }

    public function store($data){
        try{
            DB::beginTransaction();
            $dataProd = [
                'nombre' => $data['nombre'],
                'marca' => $data['marca'],
                'descripcion' => $data['descripcion'],
                'imagen' => $data['imagen'],
                'stock' => $data['stock'],
                'precio' => $data['precio']
            ];
            $producto = new Producto($dataProd);
            $producto->save();
            DB::commit();
            return 1;
        }catch (\Exception $e){
            DB::rollBack();
            return 2;
        }catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
        
    }

    public function show($id){
        return Producto::findOrFail($id);
    }

    public function update($data, $id){
        try{
            DB::beginTransaction();
           $producto = Producto::findOrFail($id);
           $producto->update($data);
            DB::commit();
            return 1;
        }catch (\Exception $e){
            DB::rollBack();
            return 2;
        }catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
    }
    public function destroy($id){
        try{
            DB::beginTransaction();

            Producto::where('id_producto', $id)->update(array('estado' => 0));
    
            DB::commit();
            return 1;
        } catch (Exception $e){
            DB::rollBack();
            return 2;   
        } catch (QueryException $ex){
            DB::rollBack();
            return 3;
        }
    }
}