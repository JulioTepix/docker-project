<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ClientesRepository;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class ClientesController extends Controller
{
    private $cliente;

    public function __construct(ClientesRepository $clientesRepository){
        $this->cliente = $clientesRepository;
    }

    public function index(Request $request){
        $search = "";
        $limit =15;
        if($request->has('search')){
            $search = $request->input('search');
            if(trim($search) != ''){
                $data = $this->cliente->search($search);
            }else{
                $data = $this->cliente->all();
            }
        }else{
            $data = $this->cliente->all();
        }
        $currentPage = Paginator::resolveCurrentPage() -1;
        $perPage = $limit;
        $currentPageSearchResults = $data->slice($currentPage * $perPage, $perPage)->all();
        $data = new LengthAwarePaginator($currentPageSearchResults, count($data), $perPage);
        return view('admin.clientes.index', ['data' => $data, 'search' => $search, 'page' => $currentPage]);
    }

    public function visitas(Request $request){
        //$search = "";
        $limit =15;
        /*if($request->has('search')){
            $search = $request->input('search');
            if(trim($search) != ''){
                $data = $this->cliente->search($search);
            }else{
                $data = $this->cliente->visitas();
            }
        }else{*/
            $data = $this->cliente->visitas();
        //}
        $currentPage = Paginator::resolveCurrentPage() -1;
        $perPage = $limit;
        $currentPageSearchResults = $data->slice($currentPage * $perPage, $perPage)->all();
        $data = new LengthAwarePaginator($currentPageSearchResults, count($data), $perPage);
        return view('admin.clientes.visitas', ['data' => $data, 'page' => $currentPage]);
    }

    public function create(){
        $data = $this->cliente->suscripciones();
        return view('admin.clientes.create', ['data' => $data]);
    }

    public function store(Request $request){
                            $dataCliente = [
                                'nombre' => ucfirst($request['nombre']),
                                'apellido_paterno' => ucfirst($request['apellido_paterno']),
                                'apellido_materno' => ucfirst($request['apellido_materno']),
                                'edad' => $request['edad'],
                                'telefono' => $request['telefono'],
                                'fecha_pago' => $request['fecha_pago'],
                                'fecha_proximo_pago' => $request['fecha_proximo_pago'],
                                'id_suscripcion' => $request['id_suscripcion']
                            ];
                            $result = $this->cliente->store($dataCliente);
                            
                            if($result == 1){
                                Session::flash('status', 'Los datos han sido guardados');
                                Session::flash('status_type', 'success');
                                return redirect()->route('clientes.index');
                            }
                            if($result == 10){
                                Session::flash('status', 'Los datos han sido guardados');
                                Session::flash('status_type', 'success');
                                return redirect()->route('clientes.visitas');
                            }
                            if($result == 2){
                                Session::flash('status', 'Problema del proceso');
                                Session::flash('status_type', 'danger');
                                return view('admin.alerts.error');
                            }
                            if($result == 3){
                                Session::flash('status', 'Problema del query');
                                Session::flash('status_type', 'danger');
                                return view('admin.alerts.error');
                            }
            
    }

    public function show($id){
        $data = $this->cliente->show($id);
        return view('admin.clientes.show',['data'=>$data]);
    }

    public function update($id){
        $data = $this->cliente->show($id);
        $data2 = $this->cliente->suscripciones();
        return view('admin.clientes.update',['data'=>$data, 'data2'=>$data2]);
    }

    public function edit(Request $request, $id){
        $dataCliente = [
            'nombre' => $request['nombre'],
            'apellido_paterno' => $request['apellido_paterno'],
            'apellido_materno' => $request['apellido_materno'],
            'edad' => $request['edad'],
            'telefono' => $request['telefono'],
            'fecha_pago' => $request['fecha_pago'],
            'fecha_proximo_pago' => $request['fecha_proximo_pago'],
            'id_suscripcion' => $request['id_suscripcion']
        ];
        $result = $this->cliente->update($dataCliente, $id);
        
        if($result == 1){
            Session::flash('status', 'Los datos han sido guardados');
            Session::flash('status_type', 'success');
            return redirect()->route('clientes.index');
        }
        if($result == 2){
            Session::flash('status', 'Problema del proceso');
            Session::flash('status_type', 'danger');
            return view('admin.alerts.error');
        }
        if($result == 3){
            Session::flash('status', 'Problema del query');
            Session::flash('status_type', 'danger');
            return view('admin.alerts.error');
        }
    }

    public function delete($id){
        $data = $this->cliente->show($id);
        return view('admin.clientes.delete',['data'=>$data]);
    }

    public function destroy(Request $request){
        $id = $request['id_cliente'];
        $result = $this->cliente->destroy($id);
        if($result == 1){
            Session::flash('status', 'La cliente fue eliminada');
            Session::flash('status_type', 'success');
            return redirect()->route('clientes.index');
        }
        if($result == 2){
            Session::flash('status', 'Problema del proceso');
            Session::flash('status_type', 'danger');
            return back()->withInput();
        }
        if($result == 3){
            Session::flash('status', 'Problema del query');
            Session::flash('status_type', 'danger');
            return back()->withInput();
        }  
    }
}