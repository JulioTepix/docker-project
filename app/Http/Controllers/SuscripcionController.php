<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SuscripcionRepository;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class SuscripcionController extends Controller
{
    private $suscripcion;

    public function __construct(SuscripcionRepository $clientesRepositor){
        $this->suscripcion = $clientesRepositor;
    }

    public function index(Request $request){
           
            $data = $this->suscripcion->all();
        
        return view('admin.suscripciones.index', ['data' => $data]);
    }

    public function create(){
        return view('admin.suscripciones.create');
    }

    public function store(Request $request){
                            $dataSS = [
                                'tipo_suscripcion' => ucfirst($request['tipo_suscripcion']),
                                'precio' => $request['precio'],
                            ];
                            $result = $this->suscripcion->store($dataSS);
                            
                            if($result == 1){
                                Session::flash('status', 'Los datos han sido guardados');
                                Session::flash('status_type', 'success');
                                return redirect()->route('suscripciones.index');
                            }
                            if($result == 2){
                                Session::flash('status', 'Problema del proceso');
                                Session::flash('status_type', 'danger');
                                return view('admin.alerts.error');
                            }
                            if($result == 3){
                                Session::flash('status', 'Problema del query');
                                Session::flash('status_type', 'danger');
                                return view('admin.alerts.error');
                            }
            
    }

    public function show($id){
        $data = $this->suscripcion->show($id);
        return view('admin.suscripciones.show',['data'=>$data]);
    }

    public function update($id){
        $data = $this->suscripcion->show($id);
        return view('admin.suscripciones.update',['data'=>$data]);
    }

    public function edit(Request $request, $id){
        $dataSS = [
            'tipo_suscripcion' => ucfirst($request['tipo_suscripcion']),
            'precio' => $request['precio'],
        ];
        $result = $this->suscripcion->update($dataSS, $id);
        
        if($result == 1){
            Session::flash('status', 'Los datos han sido guardados');
            Session::flash('status_type', 'success');
            return redirect()->route('suscripciones.index');
        }
        if($result == 2){
            Session::flash('status', 'Problema del proceso');
            Session::flash('status_type', 'danger');
            return view('admin.alerts.error');
        }
        if($result == 3){
            Session::flash('status', 'Problema del query');
            Session::flash('status_type', 'danger');
            return view('admin.alerts.error');
        }
    }

    public function delete($id){
        $data = $this->suscripcion->show($id);
        return view('admin.suscripciones.delete',['data'=>$data]);
    }

    public function destroy(Request $request){
        $id = $request['id_suscripcion'];
        $result = $this->suscripcion->destroy($id);
        if($result == 1){
            Session::flash('status', 'La suscripcion fue eliminada');
            Session::flash('status_type', 'success');
            return redirect()->route('suscripciones.index');
        }
        if($result == 2){
            Session::flash('status', 'Problema del proceso');
            Session::flash('status_type', 'danger');
            return back()->withInput();
        }
        if($result == 3){
            Session::flash('status', 'Problema del query');
            Session::flash('status_type', 'danger');
            return back()->withInput();
        }  
    }
}